﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System;

public class VertexSelection : MonoBehaviour
{
    //Draw Line
    public Material lineMaterial;
    private Vector3 pointRegionStart;
    private Vector3 pointRegionEnd;

    //select vertices
    public List<int> fixedVerticesIdx;//public interface
    private List<Vector3> fixedVertices;
    private bool isAddVertices;
    private bool isDeleteVertices;

    Mesh mesh;

    // Use this for initialization
    void Start()
    {
        pointRegionStart.Set(0.0f, 0.0f, 0.0f);
        pointRegionEnd.Set(0.0f, 0.0f, 0.0f);

        fixedVerticesIdx = new List<int>();
        fixedVertices = new List<Vector3>();
        isAddVertices = false;
        isDeleteVertices = false;

        mesh = GameObject.Find("/bunny_sim/default").GetComponent<MeshFilter>().mesh;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.GetKey(KeyCode.LeftControl))//add vertices
            {
                isAddVertices = true;
                isDeleteVertices = false;
            }
            else if(Input.GetKey(KeyCode.LeftAlt))//delete vertices
            {
                isAddVertices = false;
                isDeleteVertices = true;
            }
            pointRegionStart = Input.mousePosition;
            pointRegionEnd = pointRegionStart;
        }

        if ((Input.GetKey(KeyCode.LeftControl) && Input.GetMouseButton(0))
            || Input.GetKey(KeyCode.LeftAlt) && Input.GetMouseButton(0))
        {
            pointRegionEnd = Input.mousePosition;
        }

        if(Input.GetMouseButtonUp(0))
        {
            if (Input.GetKey(KeyCode.LeftControl)
                || Input.GetKey(KeyCode.LeftAlt))//add vertices
            {
                updateVertexSelection();
            }
        }
    }

    void OnGUI()
    {
        GUILayout.Label("X: " + Input.mousePosition.x);
        GUILayout.Label("Y: " + Input.mousePosition.y);
        GUILayout.Label("Z: " + Input.mousePosition.z);
    }

    void OnPostRender()
    {
        if (!lineMaterial)
        {
            Debug.LogError("please load line material");
            return;
        }

        if ((Input.GetKey(KeyCode.LeftControl) && Input.GetMouseButton(0))
            || Input.GetKey(KeyCode.LeftAlt) && Input.GetMouseButton(0))
        {
            Vector4 regionInfo = getRegionInfo();
            drawPointRegion(
                regionInfo[0],
                regionInfo[1],
                regionInfo[2],
                regionInfo[3],
                lineMaterial);
        }

        for (int i = 0; i < fixedVertices.Count; i++)
        {
            drawVertices(fixedVertices[i], lineMaterial);
        }
    }

    void drawPointRegion(float x, float y, float width, float height, Material mat)
    {
        GL.PushMatrix();
        mat.SetPass(0);
        GL.LoadOrtho();

        GL.Begin(GL.LINES);
        GL.Vertex3(x / Screen.width, y / Screen.height, 0);
        GL.Vertex3(x / Screen.width, (y + height) / Screen.height, 0);
        GL.End();

        GL.Begin(GL.LINES);
        GL.Vertex3(x / Screen.width, (y + height) / Screen.height, 0);
        GL.Vertex3((x + width) / Screen.width, (y + height) / Screen.height, 0);
        GL.End();

        GL.Begin(GL.LINES);
        GL.Vertex3((x + width) / Screen.width, (y + height) / Screen.height, 0);
        GL.Vertex3((x + width) / Screen.width, y / Screen.height, 0);
        GL.End();

        GL.Begin(GL.LINES);
        GL.Vertex3(x / Screen.width, y / Screen.height, 0);
        GL.Vertex3((x + width) / Screen.width, y / Screen.height, 0);
        GL.End();

        GL.PopMatrix();
    }

    void drawVertices(Vector3 v, Material mat)
    {
        Vector3 vy = v + new Vector3(0.0f, 1.0f, 0.0f);
        Vector3 vz = v + new Vector3(0.0f, 0.0f, 1.0f);

        GL.PushMatrix();
        mat.SetPass(0);

        GL.Begin(GL.LINES);
        GL.Vertex3(v.x , v.y, v.z);
        GL.Vertex3(vy.x, vy.y, vy.z);
        GL.End();

        GL.Begin(GL.LINES);
        GL.Vertex3(v.x, v.y, v.z);
        GL.Vertex3(vz.x, vz.y, vz.z);
        GL.End();

        GL.PopMatrix();
    }

    Vector4 getRegionInfo()
    {
        Vector4 regionInfo = new Vector4();

        float minX = 1000000000.0f;
        float minY = 1000000000.0f;
        float width = Mathf.Abs(pointRegionEnd.x - pointRegionStart.x);
        float height = Mathf.Abs(pointRegionEnd.y - pointRegionStart.y);

        if (pointRegionStart.x < minX)
        {
            minX = pointRegionStart.x;
        }
        if (pointRegionEnd.x < minX)
        {
            minX = pointRegionEnd.x;
        }

        if (pointRegionStart.y < minY)
        {
            minY = pointRegionStart.y;
        }
        if (pointRegionEnd.y < minY)
        {
            minY = pointRegionEnd.y;
        }

        regionInfo.Set(minX, minY, width, height);

        return regionInfo;
    }

    void updateVertexSelection()
    {
        if(!mesh)
        {
            return;
        }

        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < mesh.vertexCount; i++)
        {
            Vector3 screenPoint = Camera.main.WorldToScreenPoint(vertices[i]);

            bool InsideRect = checkInsideSelectionRegion(screenPoint);

            if (InsideRect && isAddVertices && !isDeleteVertices)
            {
                if(!fixedVerticesIdx.Exists(v1 => v1 == i))
                {
                    fixedVerticesIdx.Add(i);
                    fixedVertices.Add(vertices[i]);
                }
            }
            else if (InsideRect && !isAddVertices && isDeleteVertices)
            {
                int idx = fixedVerticesIdx.IndexOf(i);
                if (idx != -1)
                {
                    fixedVerticesIdx.RemoveAt(idx);
                    fixedVertices.RemoveAt(idx);
                }
            }
        }
    }

    bool checkInsideSelectionRegion(Vector3 screenPoint)
    {
        bool isInside = false;

        Vector4 regionInfo = getRegionInfo();

        if(screenPoint.x >= regionInfo[0]
            && screenPoint.y >= regionInfo[1]
            && screenPoint.x <= regionInfo[0] + regionInfo[2]
            && screenPoint.y <= regionInfo[1] + regionInfo[3])
        {
            isInside = true;
        }

        return isInside;
    }

    public void saveFixPoints()
    {
        FileOperation.saveFixedPoints(fixedVerticesIdx, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\fixPoints.txt");
    }
}