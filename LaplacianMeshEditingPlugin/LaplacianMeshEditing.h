#ifndef LAPLACIAN_MESH_PLUGIN_H
#define LAPLACIAN_MESH_PLUGIN_H

#if defined (EXPORTBUILD)  
# define DLLExport extern "C" __declspec (dllexport)  
# else  
# define DLLExport extern "C" __declspec (dllimport)  
#endif  

#include <iostream>
#include <vector>
#include <array>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/Handles.hh>

typedef OpenMesh::TriMesh_ArrayKernelT<> Mesh;

#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <Eigen/SparseQR>

typedef Eigen::Triplet<double> triplet;

DLLExport int add(int a, int b);

DLLExport void editing(std::array<float, 3>* vertices, int verticeSize, int* faces, int faceSize, 
	int* fixPointsIndices, int fixPointsSize,
	int* handlePointsIndices, std::array<float, 3>* handleNewPoints, int handlePointSize);

DLLExport void constructMesh(std::array<float, 3>* vertices, int verticeSize, int* faces, int faceSize, Mesh* outputMesh);

DLLExport void constructMatrixA(Mesh* mesh, Eigen::SparseMatrix<double>& matA, std::vector<triplet>& tripletList,
	int* fixPointsIndices, int fixPointsSize,
	int* handlePointsIndices, int handlePointSize);

DLLExport void constructMatrixB(Mesh* mesh, Eigen::VectorXd& vecB0, Eigen::VectorXd& vecB1, Eigen::VectorXd& vecB2,
	int* fixPointsIndices, int fixPointsSize,
	std::array<float, 3>* handleNewPoints, int handlePointSize);

DLLExport void solveEquation(Eigen::SparseMatrix<double>& matA, Eigen::VectorXd& vecB0, Eigen::VectorXd& vecB1, Eigen::VectorXd& vecB2,
	Eigen::VectorXd& vecX0, Eigen::VectorXd& vecX1, Eigen::VectorXd& vecX2);

DLLExport double getAverageWeight(int valence);

#endif