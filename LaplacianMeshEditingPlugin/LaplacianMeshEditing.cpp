#define EXPORTBUILD
#include "LaplacianMeshEditing.h"

DLLExport int add(int a, int b)
{
	return a + b;
}

DLLExport void editing(std::array<float, 3>* vertices, int verticeSize, int* faces, int faceSize,
	int* fixPointsIndices, int fixPointsSize,
	int* handlePointsIndices, std::array<float, 3>* handleNewPoints, int handlePointSize)
{
	std::cout << "verticeSize: " << verticeSize<<std::endl;
	std::cout << "faceSize: " << faceSize << std::endl;
	std::cout << "fixPointsIndices: " << fixPointsSize << std::endl;
	std::cout << "handlePointSize: " << handlePointSize << std::endl;

	//构建三维网格结构
	Mesh* originMesh = new Mesh();
	constructMesh(vertices, verticeSize, faces, faceSize, originMesh);

	//求解器
	Eigen::SparseMatrix<double> matA;
	std::vector<triplet> tripletList;
	Eigen::VectorXd vecB0;
	Eigen::VectorXd vecB1;
	Eigen::VectorXd vecB2;
	Eigen::VectorXd vecX0;
	Eigen::VectorXd vecX1;
	Eigen::VectorXd vecX2;

	//构建A矩阵
	constructMatrixA(originMesh, matA, tripletList, fixPointsIndices, fixPointsSize, handlePointsIndices, handlePointSize);

	//构建B矩阵
	constructMatrixB(originMesh, vecB0, vecB1, vecB2, fixPointsIndices, fixPointsSize, handleNewPoints, handlePointSize);

	//求解
	solveEquation(matA, vecB0, vecB1, vecB2, vecX0, vecX1, vecX2);

	//恢复网格
	//OpenMesh::IO::write_mesh(*originMesh, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\originMesh.obj");
	
	for (int i = 0; i < originMesh->n_vertices(); i++)
	{
		originMesh->point(Mesh::VertexHandle(i)).data()[0] = vecX0(i);
		originMesh->point(Mesh::VertexHandle(i)).data()[1] = vecX1(i);
		originMesh->point(Mesh::VertexHandle(i)).data()[2] = vecX2(i);
	}

	//originMesh->update_normals();
	//OpenMesh::IO::write_mesh(*originMesh, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\morphingMesh.obj");

	//保存覆盖原来的网格信息
	for (int i = 0; i < verticeSize; i++)
	{
		vertices[i].data()[0] = originMesh->point(Mesh::VertexHandle(i)).data()[0];
		vertices[i].data()[1] = originMesh->point(Mesh::VertexHandle(i)).data()[1];
		vertices[i].data()[2] = originMesh->point(Mesh::VertexHandle(i)).data()[2];
	}

	if (originMesh)
	{
		originMesh->clear();
		delete originMesh;
		originMesh = nullptr;
	}
}

DLLExport void constructMesh(std::array<float, 3>* vertices, int verticeSize, int* faces, int faceSize, Mesh* outputMesh)
{
	outputMesh->clear();

	for (int i = 0; i < verticeSize; i++)
	{
		Mesh::Point p(vertices[i].data());
		outputMesh->add_vertex(p);
	}

	std::vector<Mesh::VertexHandle> face;
	for (int i = 0; i < faceSize; i++)
	{
		face.push_back(Mesh::VertexHandle(faces[3 * i]));
		face.push_back(Mesh::VertexHandle(faces[3 * i + 1]));
		face.push_back(Mesh::VertexHandle(faces[3 * i + 2]));

		outputMesh->add_face(face);
		face.clear();
	}

	outputMesh->update_normals();
}

DLLExport void constructMatrixA(Mesh* mesh, Eigen::SparseMatrix<double>& matA, std::vector<triplet>& tripletList,
	int* fixPointsIndices, int fixPointsSize,
	int* handlePointsIndices, int handlePointSize)
{
	int columns = mesh->n_vertices();
	int rows = columns + fixPointsSize + handlePointSize;

	for (int i = 0; i < columns; i++)
	{
		Mesh::VertexHandle curVetex(i);
		int valence = mesh->valence(curVetex);

		tripletList.push_back(triplet(i, i, 1));

		for (auto vv_it = mesh->vv_begin(curVetex); vv_it != mesh->vv_end(curVetex); ++vv_it)
		{
			int vv_id = vv_it->idx();
			double vv_weight = getAverageWeight(valence);
			tripletList.push_back(triplet(i, vv_id, -vv_weight));
		}
	}

	int curRow = columns;
	for (int i = 0; i < fixPointsSize; i++)
	{
		tripletList.push_back(triplet(curRow, fixPointsIndices[i], 1));
		curRow++;
	}

	curRow = columns + fixPointsSize;
	for (int i = 0; i < handlePointSize; i++)
	{
		tripletList.push_back(triplet(curRow, handlePointsIndices[i], 1));
		curRow++;
	}

	matA.resize(rows, columns);
	matA.setFromTriplets(tripletList.begin(), tripletList.end());

	//std::cout << matA.rows() <<" "<<matA.cols()<< std::endl;
}

DLLExport void constructMatrixB(Mesh* mesh, Eigen::VectorXd& vecB0, Eigen::VectorXd& vecB1, Eigen::VectorXd& vecB2,
	int* fixPointsIndices, int fixPointsSize,
	std::array<float, 3>* handleNewPoints, int handlePointSize)
{
	int rows = mesh->n_vertices() + fixPointsSize + handlePointSize;
	vecB0.resize(rows);
	vecB1.resize(rows);
	vecB2.resize(rows);

	for (int i = 0; i < mesh->n_vertices(); i++)
	{
		Mesh::VertexHandle curVetex(i);
		int valence = mesh->valence(curVetex);
		Mesh::Point curPoint = mesh->point(curVetex);

		Mesh::Point laplacianPoint(.0, .0, .0);
		for (auto vv_it = mesh->vv_begin(curVetex); vv_it != mesh->vv_end(curVetex); ++vv_it)
		{
			Mesh::Point vvPoint = mesh->point(vv_it.handle());
			double vv_weight = getAverageWeight(valence);

			laplacianPoint += vvPoint * vv_weight;
		}

		laplacianPoint = curPoint - laplacianPoint;

		vecB0(i) = laplacianPoint[0];
		vecB1(i) = laplacianPoint[1];
		vecB2(i) = laplacianPoint[2];
	}

	int curRow = mesh->n_vertices();
	for (int i = 0; i < fixPointsSize; i++)
	{
		Mesh::Point fixPoint = mesh->point(Mesh::VertexHandle(fixPointsIndices[i]));
		vecB0(curRow) = fixPoint[0];
		vecB1(curRow) = fixPoint[1];
		vecB2(curRow) = fixPoint[2];
		curRow++;
	}

	curRow = mesh->n_vertices() + fixPointsSize;
	for (int i = 0; i < handlePointSize; i++)
	{
		vecB0(curRow) = handleNewPoints[i].data()[0];
		vecB1(curRow) = handleNewPoints[i].data()[1];
		vecB2(curRow) = handleNewPoints[i].data()[2];
		curRow++;
	}
}

DLLExport void solveEquation(Eigen::SparseMatrix<double>& matA, Eigen::VectorXd& vecB0, Eigen::VectorXd& vecB1, Eigen::VectorXd& vecB2,
	Eigen::VectorXd& vecX0, Eigen::VectorXd& vecX1, Eigen::VectorXd& vecX2)
{
	double totaltime = 0;
	double startmoment = 0;
	double finishmoment = 0;
	Eigen::SparseLU<Eigen::SparseMatrix<double, Eigen::ColMajor>, Eigen::COLAMDOrdering<int>> solver;
	//Eigen::SparseQR<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;

	Eigen::SparseMatrix<double> matAT = matA.transpose();
	Eigen::SparseMatrix<double> matATA = matAT * matA;
	Eigen::VectorXd vecATB0 = matAT * vecB0;
	Eigen::VectorXd vecATB1 = matAT * vecB1;
	Eigen::VectorXd vecATB2 = matAT * vecB2;

	//分解matA矩阵
	startmoment = clock();
	solver.analyzePattern(matATA);
	finishmoment = clock();
	totaltime += (double)(finishmoment - startmoment) / CLOCKS_PER_SEC;
	std::cout << "Solver AnalyzePattern cost:" << (double)(finishmoment - startmoment) / CLOCKS_PER_SEC << std::endl;

	startmoment = clock();
	solver.factorize(matATA);
	finishmoment = clock();
	totaltime += (double)(finishmoment - startmoment) / CLOCKS_PER_SEC;
	std::cout << "Solver Factorize cost:" << (double)(finishmoment - startmoment) / CLOCKS_PER_SEC << std::endl;

	//求解矩阵
	startmoment = clock();
	vecX0 = solver.solve(vecATB0);
	finishmoment = clock();
	totaltime += (double)(finishmoment - startmoment) / CLOCKS_PER_SEC;
	std::cout << "Solve vecX0 cost:" << (double)(finishmoment - startmoment) / CLOCKS_PER_SEC << std::endl;

	startmoment = clock();
	vecX1 = solver.solve(vecATB1);
	finishmoment = clock();
	totaltime += (double)(finishmoment - startmoment) / CLOCKS_PER_SEC;
	std::cout << "Solve vecX1 cost:" << (double)(finishmoment - startmoment) / CLOCKS_PER_SEC << std::endl;

	startmoment = clock();
	vecX2 = solver.solve(vecATB2);
	finishmoment = clock();
	totaltime += (double)(finishmoment - startmoment) / CLOCKS_PER_SEC;
	std::cout << "Solve vecX2 cost:" << (double)(finishmoment - startmoment) / CLOCKS_PER_SEC << std::endl;

}

DLLExport double getAverageWeight(int valence)
{
	return 1.0 / valence;
}