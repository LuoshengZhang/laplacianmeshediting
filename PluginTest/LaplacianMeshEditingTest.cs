﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;
using UnityEngine;

namespace PluginTest
{
    class LaplacianMeshEditingTest
    {
        [DllImport("LaplacianMeshEditingPlugin.dll", EntryPoint = "add", CallingConvention = CallingConvention.Cdecl)]
        public static extern int add(int a, int b);

        [StructLayout(LayoutKind.Explicit)]
        public class LaplacianMeshEditing
        {
            [DllImport("LaplacianMeshEditingPlugin.dll", EntryPoint = "editing", CallingConvention = CallingConvention.Cdecl)]
            public static extern
            void editing(Vector3[] vertices, int verticeSize, int[] faces, int faceSize, 
                int[] fixPointsIndices, int fixPointsSize,
                int[] handlePointsIndices, Vector3[] handleNewPoints, int handlePointSize);
        }

        public static void mainTest()
        {
            //1.读入算法所需要的数据
            Vector3[] vertices = new Vector3[1];
            int[] faces = new int[1];
            FileOperation.loadMesh(ref vertices, ref faces, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\mesh.txt");

            List<int> fixPointsIndices = new List<int>();
            FileOperation.loadFixedPoints(ref fixPointsIndices, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\fixPoints.txt");

            List<int> handlePointIndices = new List<int>();
            List<Vector3> handleNewPoints = new List<Vector3>();
            FileOperation.loadHandlePoints(ref handlePointIndices, ref handleNewPoints, "F:\\Unity5Project\\LaplacianMeshEditing\\PluginTest\\TestData\\handle.txt");

            //2.构建网格
            LaplacianMeshEditing.editing(vertices, vertices.Length, faces, faces.Length / 3, 
                fixPointsIndices.ToArray(), fixPointsIndices.Count, 
                handlePointIndices.ToArray(), handleNewPoints.ToArray(), handlePointIndices.Count);

        }

    }
}
