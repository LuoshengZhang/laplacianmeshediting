﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

using System.Collections.Generic;

public class FileOperation : MonoBehaviour {

    public static void saveMesh(Vector3[] vertices, int[] faces, string absoluteFileName)
    {
        FileStream fs = new FileStream(absoluteFileName, FileMode.Create);
        StreamWriter sw = new StreamWriter(fs);

        sw.Write(vertices.Length + "\n");
        for(int i = 0; i < vertices.Length; i++)
        {
            sw.Write(vertices[i].x + " " + vertices[i].y + " " + vertices[i].z + "\n");
        }
        
        sw.Write(faces.Length + "\n");
        for (int i = 0; i < faces.Length; i++)
        {
            sw.Write(faces[i] + "\n");
        }

        sw.Flush();
        sw.Close();
        fs.Close();
    }

    public static void loadMesh(ref Vector3[] vertices, ref int[] faces, string absoluteFileName)
    {
        StreamReader sr = new StreamReader(absoluteFileName, Encoding.Default);

        int verticeSize = int.Parse(sr.ReadLine());
        vertices = new Vector3[verticeSize];

        for(int i = 0; i < verticeSize; i ++)
        {
            string[] xyzStr = sr.ReadLine().Split(' ');
            vertices[i].x = float.Parse(xyzStr[0]);
            vertices[i].y = float.Parse(xyzStr[1]);
            vertices[i].z = float.Parse(xyzStr[2]);
        }

        int faceSize = int.Parse(sr.ReadLine());
        faces = new int[faceSize];

        for(int i = 0; i < faceSize; i++)
        {
            faces[i] = int.Parse(sr.ReadLine());
        }

        sr.Close();
    }

    public static void saveFixedPoints(List<int> fixedPoints, string absoluteFileName)
    {
        FileStream fs = new FileStream(absoluteFileName, FileMode.Create);
        StreamWriter sw = new StreamWriter(fs);

        sw.Write("FixPoints: " + fixedPoints.Count + "\n");
        for (int i = 0; i < fixedPoints.Count; i++)
        {
            sw.Write(fixedPoints[i] + "\n");
        }

        sw.Flush();
        sw.Close();
        fs.Close();
    }

    public static void loadFixedPoints(ref List<int> fixPoints, string absoluteFileName)
    {
        StreamReader sr = new StreamReader(absoluteFileName, Encoding.Default);

        string[] firstStrings = sr.ReadLine().Split(' ');
        int fixPointSize = int.Parse(firstStrings[1]);

        for(int i = 0; i < fixPointSize; i++)
        {
            fixPoints.Add(int.Parse(sr.ReadLine()));
        }

        sr.Close();
    }

    public static void saveHandlePoints(List<int> handleIndices, List<Vector3> handlePositions, string absoluteFileName)
    {
        FileStream fs = new FileStream(absoluteFileName, FileMode.Create);
        StreamWriter sw = new StreamWriter(fs);

        sw.Write("handles: " + handleIndices.Count + "\n");
        for (int i = 0; i < handleIndices.Count; i++)
        {
            sw.Write(handleIndices[i] + " " 
                + handlePositions[i].x + " "
                + handlePositions[i].y + " " 
                + handlePositions[i].z + "\n");
        }

        sw.Flush();
        sw.Close();
        fs.Close();
    }

    public static void loadHandlePoints(ref List<int> handleIndices, ref List<Vector3> handlePositions, string absoluteFileName)
    {
        StreamReader sr = new StreamReader(absoluteFileName, Encoding.Default);

        string[] firstStrings = sr.ReadLine().Split(' ');
        int handleSize = int.Parse(firstStrings[1]);

        for (int i = 0; i < handleSize; i++)
        {
            string[] handleStr = sr.ReadLine().Split(' ');
            handleIndices.Add(int.Parse(handleStr[0]));

            handlePositions.Add(new Vector3(float.Parse(handleStr[1]), float.Parse(handleStr[2]), float.Parse(handleStr[3])));
        }

        sr.Close();
    }

}
